import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.io.Source

object task3 {

  def text_input(input_string: String, input_map: mutable.Map[String, Int], stopwords_list: List[String]): mutable.Map[String, Int] = {

    val sublist = new ListBuffer[String]()
    val list: List[String] = input_string.split(' ').toList
    for (element <- list) {
      val cleaned_element = element.replaceAll("[^A-Za-z0-9']", "")
      if (cleaned_element.nonEmpty) {
        sublist += cleaned_element
      }
    }
    val clean_list = sublist.toList
    val final_list = clean_list diff stopwords_list

    val count_map = final_list.groupBy(identity).mapValues(_.size)
    val new_map = input_map ++ count_map.map { case (k, v) => k -> (v + input_map.getOrElse(k, 0)) }

    new_map
  }

  def text_input_file(input_file: String, input_map: mutable.Map[String, Int], stopwords_list: List[String]): mutable.Map[String, Int] = {

    val text_list = new ListBuffer[String]()
    val lines = Source.fromFile(input_file).getLines.toList
    for(line<-lines){
      val small_list: List[String] = line.split(' ').toList
      for(element<-small_list) {
        val cleaned_element = element.replaceAll("[^A-Za-z0-9']", "")
        if (cleaned_element.nonEmpty) {
          text_list += cleaned_element
        }
      }
    }
    val clean_list = text_list.toList
    val final_list = clean_list diff stopwords_list

    val count_map = final_list.groupBy(identity).mapValues(_.size)

    val new_map = input_map ++ count_map.map { case (k, v) => k -> (v + input_map.getOrElse(k, 0)) }

    new_map
  }

  def word_cloud_output_console(original_map: mutable.Map[String, Int], n:Int): Unit = {
    println(original_map.toSeq.sortBy(_._2)(Ordering[Int].reverse).take(n).toMap.keys)
  }

  def word_cloud_output_to_file(original_map: mutable.Map[String, Int], n:Int): Unit ={
    val csv_file = "word_cloud.csv"
    val writer = new BufferedWriter(new FileWriter(csv_file))
    for(el_to_write<-original_map.toSeq.sortBy(_._2)(Ordering[Int].reverse).take(n).toMap.keys){
      writer.write(el_to_write)
      writer.write(" ")
      writer.write(String.valueOf(original_map.get(el_to_write).get))
      writer.write("\n")
    }
    writer.close()
  }

  def main(args: Array[String]) = {

    var my_map = collection.mutable.Map[String, Int]()

    val stopwords_filename = "stop_words_english.txt"
    val stopwords_list = Source.fromFile(stopwords_filename).getLines.toList

    while(true){
      println(my_map)
      println("Choose what you want to do:")
      println("1) Give pure string")
      println("2) Give file")
      println("3) Print selected number of most frequent words")
      println("4) Write selected number of most frequent words to csv file")
      try {
        val decision = scala.io.StdIn.readLine().toInt
        if(decision == 1){
          println("1) Give pure string:")
          my_map = text_input(scala.io.StdIn.readLine().toString, my_map, stopwords_list)
        }
        else if(decision == 2){
          println("2) Give path to string file:")
          my_map = text_input_file(scala.io.StdIn.readLine().toString, my_map, stopwords_list)
        }
        else if(decision == 3){
          println("3) Give number:")
          word_cloud_output_console(my_map, scala.io.StdIn.readLine().toInt)
        }
        else if(decision == 4){
          println("4) Give number:")
          word_cloud_output_to_file(my_map, scala.io.StdIn.readLine().toInt)
        }
        else {
          println("Bad choice - choose number from 1 to 4")
        }
      } catch {
        case e: NumberFormatException => println("Bad choice - give number, not string")
      }
    }

  }
}