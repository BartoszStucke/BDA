import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
import scala.collection.mutable.ListBuffer

object task2 {
  def main(args: Array[String]) = {

    val filename = "book.txt"
    val text_list = new ListBuffer[String]()
    val lines = Source.fromFile(filename).getLines.toList
    for(line<-lines){
      val small_list: List[String] = line.split(' ').toList
      for(element<-small_list) {
        val cleaned_element = element.replaceAll("[^A-Za-z0-9']", "")
        if (cleaned_element.nonEmpty) {
          text_list += cleaned_element
        }
      }
    }
    val clean_list = text_list.toList

    val stopwords_filename = "stop_words_english.txt"
    val stopwords_list = Source.fromFile(stopwords_filename).getLines.toList
    val final_list = clean_list diff stopwords_list

    val words_counter = final_list.groupBy(identity).mapValues(_.size).toSeq.sortBy(_._2)(Ordering[Int].reverse)

    val wordcloud_filename = "wourd_count.txt"
    val file = new File(wordcloud_filename)
    val writer = new BufferedWriter(new FileWriter(file))
    for(el_to_write<-words_counter.take(100).toMap.keys){
      writer.write(el_to_write)
      writer.write("\n")
    }
    writer.close()

  }
}