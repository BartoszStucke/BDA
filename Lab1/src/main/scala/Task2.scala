object Task2 {

    def main(args: Array[String]): Unit = {
        val nodesAndEdges = List[(Int, List[Int])](
            (1, List(2, 3)),
            (3, List(1, 5)),
            (2, List(5)),
            (5, List()),
        )

        val w1 = new Worker
        val w2 = new Worker
        val w3 = new Worker

        val reducer = new Reducer

        reducer.reduce(w1.map(nodesAndEdges.head))
        reducer.reduce(w2.map(nodesAndEdges(1)))
        reducer.reduce(w3.map(nodesAndEdges.slice(2,4)))
        reducer.printGraph()
    }
}