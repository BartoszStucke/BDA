import scala.collection.mutable.ListBuffer
import scala.io.Source

class DocumentHandler(val documentList: List[String]) {

    private val CWD = System.getProperty("user.dir") + "/src/main/texts/"

    private val stopwords_filename = CWD + "stopwords.txt"

    def loadDocuments(): List[Document] = {
        loadWords().toList
    }

    private def loadWords(): ListBuffer[Document] = {
        val documents = ListBuffer[Document]()

        documentList.foreach(documentInfo => {
            var wordsList = List[String]()
            val documentLines = getDocumentLines(CWD + documentInfo)

            wordsList = processText(documentLines)
            val wordsCount = countWords(wordsList)
            documents += new Document(wordsList, wordsCount, documentInfo)}
        )
        documents
    }

    private def getDocumentLines(file_path: String): List[String] = {
        val source = Source.fromFile(file_path)
        try source.getLines.toList finally source.close
    }

    private def processText(documentLines: List[String]): List[String] = {
        var word_list = removePunctuation(documentLines)
        word_list = removeStopWords(word_list)
        word_list
    }

    private def removePunctuation(documentLines: List[String]): List[String] = {
        var wordsList = ListBuffer[String]()
        for (line <- documentLines) {
            for (words <- line.split(' ').toList) {
                val cleaned = words.replaceAll("""[\p{Punct}]""", "")
                if (cleaned.nonEmpty) wordsList += words
            }
        }
        wordsList.toList
    }

    private def removeStopWords(wordsList: List[String]): List[String] ={
        val stopWordsList = getDocumentLines(stopwords_filename)
        wordsList.filter(!stopWordsList.contains(_))
    }

    private def countWords(wordsList: List[String]): Map[String, Int] = {
        Map(wordsList.groupBy(w => w)
            .map(t => (t._1, t._2.length))
            .toSeq.sortWith(_._2 > _._2):_*)
    }

}