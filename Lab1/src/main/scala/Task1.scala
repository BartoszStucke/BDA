import scala.collection.immutable.ListMap

object Task1 {

    def main(args: Array[String]): Unit = {
        val documentsList = List[String](
            "text1.txt", "text2.txt",
        )
        val documentHandler = new DocumentHandler(documentsList)
        val documents = documentHandler.loadDocuments()

        printMostFrequentInAllDocuments(50, documents)
        val tfidf = new TFIDF(documents)
        tfidf.getTFIDFMap().foreach(println(_))
    }

    private def printMostFrequentInAllDocuments(n: Int, documents: List[Document]): Unit = {
        println(Console.BOLD + n + " most frequent words in all documents" + Console.RESET)
        documents.foreach(document => {
            println(Console.BOLD + f"Document: ${document.documentName}" + Console.RESET)
            ListMap(document.wordsCountMap
                .toSeq.sortWith(_._2 > _._2):_*)
                .take(n).foreach(p => println(f"${p._1}: ${p._2}"))
        })
    }
}