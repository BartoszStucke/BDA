import scala.collection.mutable.ListBuffer

class Worker {
    def map(list: List[(Int, List[Int])]): List[(Int, Int)] = {
        var invertedEdges = ListBuffer[(Int, Int)]()
        list.foreach(tuple => {
            invertedEdges = invertedEdges ++ map(tuple)
        })
        invertedEdges.toList
    }

    def map(tuple: (Int, List[Int])): List[(Int, Int)] = {
        val invertedEdges = ListBuffer[(Int, Int)]()
        tuple._2.foreach(edge => {
            invertedEdges.append((edge, tuple._1))
        })
        invertedEdges.toList
    }
}
