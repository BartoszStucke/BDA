import scala.collection.mutable.ListBuffer

class TFIDF(val documents: List[Document]) {

    def getTFIDFMap(): Map[String, Map[String, Double]] = {
        val tfValues = computeTF()
        val idfValues = computeIDF()
        val tfidf = collection.mutable.Map[String, Map[String, Double]]()
        documents.foreach(document => {
            val documentTF = tfValues(document.documentName)
            tfidf += (document.documentName -> document
                .wordsCountMap
                .map(p => (p._1, documentTF(p._1) * idfValues(p._1))))
        })
        tfidf.toMap
    }

    private def computeTF(): collection.mutable.Map[String, Map[String, Double]] = {
        val tfValues = collection.mutable.Map[String, Map[String, Double]]()
        documents.foreach(document => {
            val wordsCount = document.wordsCountMap
            tfValues += (document.documentName -> wordsCount
                .map(p => (p._1, p._2.asInstanceOf[Double] / document.allWordsList.length)))
        })
        tfValues
    }

    def computeIDF(): Map[String, Double] = {
        val setsOfDocumentsWords = ListBuffer[Set[String]]()

        documents.foreach(document => {
            setsOfDocumentsWords.addOne(document.allWordsList.toSet)
        })

        val allWordsSet = collection.mutable.Set[String]()

        documents.foreach(document => {
            document.allWordsList.toSet.foreach(allWordsSet.add)
        })

        allWordsSet.map(word => (word, Math.log(documents
            .length / getNumberOfOccurrences(word, setsOfDocumentsWords)))).toMap
    }

    private def getNumberOfOccurrences(p: String, wordSets: ListBuffer[Set[String]]): Int = {
        var occurrences = 0
        wordSets.foreach(set => {
            if (set.contains(p))
                occurrences += 1
        })
        occurrences
    }


}