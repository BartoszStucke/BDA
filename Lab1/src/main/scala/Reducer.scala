import scala.collection.mutable.{ListBuffer, Map}


class Reducer {
    private val invertedGraph = Map[Int, ListBuffer[Int]]()

    def printGraph(): Unit = {
        println()
        invertedGraph.foreach(tuple => {
            var str = ""
            tuple._2.foreach(edge => {
                str = str + ", " + edge
            })
            println(f"Node: ${tuple._1}, edges: [${str.dropWhile(_ == ',').dropWhile(_ == ' ')}]")
        })
    }

    def reduce(workerOutput: List[(Int, Int)]): Unit = {
        workerOutput.foreach(tuple => {
            invertedGraph.updateWith(tuple._1) {
                case Some(edgesList) => Some(edgesList.append(tuple._2))
                case None => Some(ListBuffer[Int](tuple._2))
            }
        })
    }
}
